package com.criticalgnome.storageaccesframeworkdemo

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        openButton.setOnClickListener {
            startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = TYPE_ALL
                putExtra(Intent.EXTRA_MIME_TYPES, arrayOf(TYPE_JPEG, TYPE_PDF))
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
            }, REQUEST_CODE_OPEN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_OPEN && resultCode == Activity.RESULT_OK) {
            data?.data?.let { uri ->
                when (contentResolver.getType(uri)) {
                    TYPE_JPEG -> image.setImageBitmap(getBitmapFromUri(uri))
                    TYPE_PDF -> {
                        val pdfViewIntent = Intent(Intent.ACTION_VIEW)
                        pdfViewIntent.setDataAndType(uri, TYPE_PDF)
                        pdfViewIntent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                        val intent = Intent.createChooser(pdfViewIntent, "PDF")
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun dumpImageMetaData(uri: Uri) {
        contentResolver.query(uri, null, null, null, null, null).use { cursor ->
            if (cursor != null && cursor.moveToFirst()) {
                val displayName: String = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
                val size: String = if (!cursor.isNull(sizeIndex)) cursor.getString(sizeIndex) else "Unknown"
                val mimeType = contentResolver.getType(uri)
                AlertDialog.Builder(this)
                    .setMessage("Name: $displayName\nSize: $size\nMimeType: $mimeType")
                    .setPositiveButton(android.R.string.ok) { _, _ -> /* No op */ }
                    .show()
            }
        }
    }

    private fun getBitmapFromUri(uri: Uri) =
        contentResolver.openFileDescriptor(uri, MODE_READ).use {
            it?.let { BitmapFactory.decodeFileDescriptor(it.fileDescriptor) }
        }

    private fun readBytesFromUri(uri: Uri) =
        contentResolver.openInputStream(uri)?.readBytes()

    companion object {
        private const val REQUEST_CODE_OPEN = 101
        private const val TYPE_ALL = "*/*"
        private const val TYPE_JPEG = "image/jpeg"
        private const val TYPE_PDF = "application/pdf"
        private const val MODE_READ = "r"
    }
}